#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/9 14:32
# @Author  : Zy
# @File    : main.py
# @Software: PyCharm
from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy as By


class TestFind():
    # 设置caps的值
    def setup(self):
        self.desire_cap = {
            # 默认是Android
            "platformName": "android",
            # adb devices的sn名称
            "deviceName": "ac52df0f",
            # 包名
            "appPackage": "com.xueqiu.android",
            # activity名字
            "appActivity": ".view.WelcomeActivityAlias",
            "noReset": 'true',
            "unicodeKeyboard": 'true',
            "resetKeyboard": 'true',
            "skipDeviceInitialization": 'true'

        }
        # 运行appium，前提是要打开appium server
        self.driver = webdriver.Remote(
            "http://127.0.0.1:4723/wd/hub", self.desire_cap)
        self.driver.implicitly_wait(10)

    def test_search(self):
        """
        1.打开雪球app
        2.点击搜索输入框
        3.向搜索输入框输入“阿里巴巴”
        4.在搜索的结果里选择阿里巴巴，然后点击
        5.获取这只上香港 阿里巴巴的股价，并判断这只股价的价格>200
        :return:
        """
        sleep(3)
        # 点击搜索框
        self.driver.find_element(
            By.ID, "com.xueqiu.android:id/home_search").click()
        # 向搜索框输入阿里巴巴
        self.driver.find_element(
            By.ID, "com.xueqiu.android:id/search_input_text").send_keys("阿里巴巴")
        # 找到搜索框预览结果的阿里巴巴，并点击
        self.driver.find_element(
            By.XPATH,
            "//*[@resource-id='com.xueqiu.android:id/name' and @text='阿里巴巴']").click()
        # 选择HK股价的元素
        prices = self.driver.find_elements(
            By.ID, "com.xueqiu.android:id/current_price")[1]
        # 提取股价的text属性
        price = float(prices.text)

        # 判断股价是否大于200
        assert price > 200

    def test_xpath(self):
        sleep(3)
        # 点击搜索框
        self.driver.find_element(
            By.ID, "com.xueqiu.android:id/home_search").click()
        # 向搜索框输入阿里巴巴
        self.driver.find_element(
            By.ID, "com.xueqiu.android:id/search_input_text").send_keys("阿里巴巴")
        # 找到搜索框预览结果的阿里巴巴，并点击
        self.driver.find_element(
            By.XPATH,
            "//*[@resource-id='com.xueqiu.android:id/name' and @text='阿里巴巴']").click()
        # 选择HK股价的元素
        prices = self.driver.find_element(
            By.XPATH, "//*[@text='09988']/../../..//*[@resource-id='com.xueqiu.android:id/current_price']")
        # 提取股价的text属性
        price = float(prices.text)

        # 判断股价是否大于200
        assert price > 200

    def test_uiautomator(self):
        '''
        1.  点击我的 , 进入个人信息页面
        2.  点击登录 , 进入登录页面
        3. 输入用户名 , 输入密码
        4. 点击登录
        :return:
        '''
        # self.driver.find_element_by_android_uiautomator(
        #     'new UiSelector().text("我的")').click()
        self.driver.find_element_by_android_uiautomator(
            'new UiSelector().resourceId("com.xueqiu.android:id/tab_name").text("我的")').click()
        self.driver.find_element_by_xpath(
            '//*[@resource-id="com.xueqiu.android:id/tab_name" and @text="我的"]')
        self.driver.find_element_by_android_uiautomator(
            'new UiSelector().textContains("帐号密码登录")').click()
        self.driver.find_element_by_android_uiautomator(
            'new UiSelector().resourceId("com.xueqiu.android:id/login_account")').send_keys('15895030957')
        self.driver.find_element_by_android_uiautomator(
            'new UiSelector().resourceId("com.xueqiu.android:id/login_password")').send_keys('lyznxf123')
        self.driver.find_element_by_android_uiautomator(
            'new UiSelector().resourceId("com.xueqiu.android:id/button_next")').click()

    def test_scroll(self):
        self.driver.find_element_by_android_uiautomator(
            'new UiSelector().text("关注")').click()
        self.driver.find_element_by_android_uiautomator(
            'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains("雪球路演").instance(0));').click()