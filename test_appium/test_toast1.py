#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/23 15:29
# @Author  : Zy
# @File    : test_toast1.py
# @Software: PyCharm
import pytest
import os
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from hamcrest import *


class TestToast():
    # 设置caps的值
    def setup(self):
        self.desire_cap = {
            # 默认是Android
            "platformName": "android",
            # adb devices的sn名称
            "deviceName": "ac52df0f",
            # 包名
            # "appPackage": "io.appium.android.apis",
            # activity名字
            # "appActivity": "io.appium.android.apis.view.PopupMenu1",
            "appPackage": "com.xueqiu.android",
            # activity名字
            "appActivity": ".view.WelcomeActivityAlias",
            "noReset": 'true',
            "unicodeKeyboard": 'true',
            "resetKeyboard": 'true',
            "skipDeviceInitialization": 'true',
            "dontStopAppOnReset": 'true'

        }
        # 运行appium，前提是要打开appium server
        self.driver = webdriver.Remote(
            "http://127.0.0.1:4723/wd/hub", self.desire_cap)
        self.driver.implicitly_wait(10)
    def teardown(self):
        self.driver.find_element_by_id('com.xueqiu.android:id/action_close').click()
    def teardown_class(self):
        try:
            os.system('adb shell ime set com.baidu.input_yijia/.ImeService')
        except Exception as e:
            print(e)

    def test_toast(self):
        self.driver.find_element(
            MobileBy.ACCESSIBILITY_ID,
            'Make a Popup!').click()
        self.driver.find_element(MobileBy.XPATH, '//*[@text="Search"]').click()
        print(self.driver.page_source)
        print(
            self.driver.find_element(
                MobileBy.XPATH,
                '//*[contains(@text, "Clicked popup")]').text)

    def test_hamcrest(self):
        assert_that(10, equal_to(10))

    def test_search(self):
        '''
        1. 打开 雪球应用
        2. 点击 搜索框
        3. 输入 搜索词 'alibabba'  or '小米'
        4 判断股票价格
        :return:
        '''
        self.driver.find_element(
            MobileBy.ID, "com.xueqiu.android:id/home_search").click()
        # 向搜索框输入阿里巴巴
        self.driver.find_element(
            MobileBy.ID, "com.xueqiu.android:id/search_input_text").send_keys("阿里巴巴")
        self.driver.find_element(
            MobileBy.ID, 'com.xueqiu.android:id/name').click()
        current_price = float(self.driver.find_element_by_xpath(
            '//*[@text="阿里巴巴"]/../..//*[@resource-id="com.xueqiu.android:id/current_price"]').text)
        expect_price = 270
        assert_that(current_price, close_to(expect_price, expect_price * 0.1))
        self.driver.swipe()