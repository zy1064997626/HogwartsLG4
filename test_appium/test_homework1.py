#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/27 17:17
# @Author  : Zy
# @File    : test_homework1.py
# @Software: PyCharm
'''
删除 微信联系人
'''
import os
import time

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction


class TestHomeWork:
    def setup(self):
        _appPackage = 'com.tencent.wework'
        _appActivity = "com.tencent.wework.launch.LaunchSplashActivity"
        self.desire_cap = {
            # 默认是Android
            "platformName": "android",
            # adb devices的sn名称
            "deviceName": "ac52df0f",
            # 包名
            "appPackage": _appPackage,
            # activity名字
            "appActivity": _appActivity,
            "noReset": 'true',
            "unicodeKeyboard": 'true',
            "resetKeyboard": 'true',
            # "skipDeviceInitialization": 'true',
            'autoGrantPermissions': 'true',
            "dontStopAppOnReset": 'true'
        }
        self._driver = webdriver.Remote(
            "http://127.0.0.1:4723/wd/hub", self.desire_cap)
        self._driver.implicitly_wait(10)

    def teardown_class(self):
        try:
            os.system('adb shell ime set com.baidu.input_yijia/.ImeService')
        except Exception as e:
            print(e)

    def test_del(self):
        '''
        删除联系人
        :return:
        '''
        # 点击
        self._driver.find_element(
            MobileBy.ID, 'com.tencent.wework:id/i6n').click()
        # 输入文字
        self._driver.find_element(
            MobileBy.CLASS_NAME,
            'android.widget.EditText').send_keys('曹华')
        # 判断判断检查结果
        eles = self._driver.find_elements_by_xpath('//*[@resource-id="com.tencent.wework:id/gqx"]/android.widget.RelativeLayout[2]')
        eles[0].click()

        self._driver.find_element_by_id('com.tencent.wework:id/i6d').click()
        self._driver.find_element_by_id('com.tencent.wework:id/em3').click()
        self._driver.find_element_by_id('com.tencent.wework:id/i6d').click()
        self._driver.find_element_by_id('com.tencent.wework:id/b_x').click()
        self._driver.find_element_by_id('com.tencent.wework:id/elh').click()
        self._driver.find_element_by_id('com.tencent.wework:id/blx').click()
        print(self._driver.page_source)


