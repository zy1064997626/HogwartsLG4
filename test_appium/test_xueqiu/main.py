#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/24 18:09
# @Author  : Zy
# @File    : main.py
# @Software: PyCharm
from test_xueqiu.app import App

if __name__ == '__main__':
    App().start().goto_indexpage().goto_market()