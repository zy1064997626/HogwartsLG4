#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/24 13:26
# @Author  : Zy
# @File    : base_page.py
# @Software: PyCharm
import os

from appium.webdriver.webdriver import WebDriver
from appium.webdriver.common.mobileby import MobileBy
from typing import Union, Iterable, List, Dict
import yaml
from functools import lru_cache, wraps
import logging

from selenium.common.exceptions import NoSuchElementException


def handler_black(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        self = args[0]
        try:
            return func(*args, **kwargs)
        except NoSuchElementException as e:
            self._error_count += 1
            if self._error_count >= self._error_max:
                raise e
            for black in self._black_list:
                elements = self._driver.find_elements(*black)
                if elements:
                    elements[0].click()
                    return wrapper(*args, **kwargs)
    return wrapper


class BasePage:
    _black_list = []
    _error_count = 0
    _error_max = 10
    _params = {}
    LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)

    def __init__(self, driver: WebDriver = None):
        # 不写在类属性原因 ， 他是可变的
        self._driver = driver

    @handler_black
    def find(self, by, locator=None):
        element = self._driver.find_element(*by) if isinstance(by, tuple) else self._driver.find_element(by, locator)
        self._error_count = 0
        return element

    @staticmethod
    @lru_cache(maxsize=1)
    def get_by():
        return [v for obj in MobileBy.__mro__ for k,
                v in obj.__dict__.items() if not k.startswith('__')]

    @handler_black
    def send(self, ele, value):
        return ele.send_keys(value)

    def steps(self, path):
        # 判断是否传入文件流
        close_source = False
        if not hasattr(path, 'read'):
            path = open(path, encoding='utf-8')
            close_source = True
        steps: List[Dict] = yaml.safe_load(path)
        if close_source:
            path.close()
        for step in steps:
            by = step.get('by')
            locator = step.get('locator')
            action = step.get('action')
            value = step.get('value')
            logging.info(step)
            if by in self.get_by() and locator:
                ele = self.find(by, locator)
                if action == 'click':
                    ele.click()
                elif action == 'send' and value:
                    for parm in self._params:
                        value = value.replace(
                            '{%s}' %
                            param, self._params[parm])
                    # 可能会条框
                    self.send(ele, value)
                else:
                    logging.error(step)


if __name__ == '__main__':
    print(BasePage.get_by())
