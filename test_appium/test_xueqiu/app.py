#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/24 15:35
# @Author  : Zy
# @File    : app.py
# @Software: PyCharm
from appium import webdriver

from test_appium.test_xueqiu.base_page import BasePage
from test_appium.test_xueqiu.index_page import IndexPage


class App(BasePage):


    def start(self):
        _appPackage = 'com.xueqiu.android'
        _appActivity = ".view.WelcomeActivityAlias"
        if not self._driver:
            self.desire_cap = {
                # 默认是Android
                "platformName": "android",
                # adb devices的sn名称
                "deviceName": "ac52df0f",
                # 包名
                "appPackage": _appPackage,
                # activity名字
                "appActivity": _appActivity,
                "noReset": 'true',
                "unicodeKeyboard": 'true',
                "resetKeyboard": 'true',
                # "skipDeviceInitialization": 'true'
                'autoGrantPermissions': 'true'
            }
            self._driver = webdriver.Remote(
                "http://127.0.0.1:4723/wd/hub", self.desire_cap)
        else:
            self._driver.start_activity(_appPackage, _appActivity)
        self._driver.implicitly_wait(10)
        return self

    def goto_indexpage(self):
        return IndexPage(self._driver)

