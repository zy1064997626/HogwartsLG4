#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/24 17:21
# @Author  : Zy
# @File    : index_page.py
# @Software: PyCharm
from test_xueqiu.base_page import BasePage
import os

class IndexPage(BasePage):
    def goto_market(self):
        self.steps('./goto_market.yaml')

