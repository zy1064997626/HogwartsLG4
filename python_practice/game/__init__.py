#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/10/26 12:31
# @Author  : Zy
# @File    : __init__.py.py
# @Software: PyCharm
import abc

class Animal(abc.ABC):


    @abc.abstractmethod
    def speak(self):
        pass

class Dog(Animal):
    def speak(self):
        print("汪汪...")

class Cat(Animal):
    pass
if __name__ == '__main__':
    dog = Dog()
    dog.speak()
    cat = Cat()
    # cat.speak()
