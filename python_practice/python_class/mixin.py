#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/10/26 13:34
# @Author  : Zy
# @File    : mixin.py
# @Software: PyCharm
'''
使用Mixin类实现多重继承要遵循以下几个规范

责任明确：必须表示某一种功能，而不是某个物品；

功能单一：若有多个功能，那就写多个Mixin类；

绝对独立：不能依赖于子类的实现；子类即便没有继承这个Mixin类，也照样可以工作，就是缺少了某个功能。
'''
class Vehicle(object):
    pass


class PlaneMixin(object):
    def fly(self):
        print('I am flying')


class Airplane(Vehicle, PlaneMixin):
    pass
