#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/6 13:33
# @Author  : Zy
# @File    : test_weixin.py
# @Software: PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import shelve
import time
import os


class TestWork:

    def test_get_cookie(self):
        options = webdriver.ChromeOptions()
        options.debugger_address = '127.0.0.1:9222'
        driver = webdriver.Chrome(options=options)
        cookies = driver.get_cookies()
        print(cookies)
        with shelve.open('cookies') as db:
            db['cookies'] = cookies

    def test_import_contacts(self):
        # 打开无痕页面
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(2)
        # 要添加cookie前输入网址
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
        with shelve.open('cookies') as db:
            cookies = db['cookies']
        for cookie in cookies:
            self.driver.add_cookie(cookie)

        # 刷新

        self.driver.refresh()
        # 点击[添加成员]
        self.driver.find_element_by_css_selector(
            '.index_service_cnt_itemWrap:nth-child(2)').click()
        # 点击上传
        self.driver.find_element_by_css_selector('.ww_fileImporter_fileContainer_uploadInputMask').send_keys(
            os.path.join(os.path.dirname(__file__), 'zhuyuan.xlsx'))
        result = self.driver.find_element(
            By.CSS_SELECTOR, ".ww_fileImporter_fileContainer_fileNames").text
        assert 'zhuyuan.xlsx' == result
        time.sleep(5)
        self.driver.quit()
