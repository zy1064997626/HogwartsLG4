#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/7 19:17
# @Author  : Administrator
# @File    : register.py
# @Software: PyCharm
from selenium.webdriver.common.by import By

from test_selenium.test_weixin.page.base import Base


class Register(Base):
    url = 'https://work.weixin.qq.com/wework_admin/register_wx?from=myhome'

    def register(self, corp_name):
        self._driver.find_element(By.ID, 'corp_name').send_keys(corp_name)
        self._driver.find_element(By.ID, 'submit_btn').click()
        return self

    def get_error_msg(self):
        res = []
        for ele in self._driver.find_elements(
                By.CSS_SELECTOR, '.js_error_msg'):
            res.append(ele.text)
        return res


if __name__ == '__main__':
    register = Register()
    print(register.register('zhuyuan').get_error_msg())
