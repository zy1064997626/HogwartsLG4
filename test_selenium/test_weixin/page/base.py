#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/7 19:07
# @Author  : Administrator
# @File    : base.py
# @Software: PyCharm
from selenium import webdriver
import time


class Base:
    url = ''

    def __init__(self, driver: webdriver = None):
        self._driver = webdriver
        if not driver:
            self._driver = webdriver.Chrome()
        if self.url:
            self._driver.get(self.url)
        self._driver.implicitly_wait(3)

    def close(self):
        time.sleep(10)
        self._driver.quit()
