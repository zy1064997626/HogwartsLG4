#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/17 14:49
# @Author  : Zy
# @File    : data_faker.py
# @Software: PyCharm
from faker import Faker


class F:
    def __init__(self):
        # 选择中文
        self.fake = Faker("zh_CN")

    def get_name(self):
        print(self.fake.name())
        return self.fake.name()
    def get_account(self):
        print(self.fake.user_name())
        return self.fake.user_name()
    def get_phone_number(self):
        print(self.fake.phone_number())
        return self.fake.phone_number()


if __name__ == "__main__":
    test = F()
    test.get_phone_number()

