#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/17 14:40
# @Author  : Zy
# @File    : test_contact.py
# @Software: PyCharm

import pytest

from test_selenium.test_weixin1.page.index_page import IndexPage
from test_selenium.test_weixin1.utils.data_faker import F

f = F()


class TestContact:
    # 每次进入开始页面
    def setup(self):
        self.index = IndexPage()


    @pytest.mark.parametrize('name,account,phonenum', [
        [f.get_name(), f.get_account(), f.get_phone_number()]for _ in range(21)
    ])
    def test_addmember(self, name, account, phonenum):
        add_member_page = self.index.click_add_memeber()
        add_member_page.add_member(name, account, phonenum)
        result = add_member_page.get_member(name)
        assert result