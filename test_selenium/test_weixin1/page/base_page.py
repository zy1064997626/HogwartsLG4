#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/17 13:39
# @Author  : Zy
# @File    : base_page.py
# @Software: PyCharm
from selenium.webdriver.remote.webdriver import WebDriver
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    _base_url = ''

    def __init__(self, driver: WebDriver = None):
        if not driver:
            options = webdriver.ChromeOptions()
            options.debugger_address = '127.0.0.1:9222'
            self.driver = webdriver.Chrome(options=options)
        else:
            self.driver = driver
        self.driver.implicitly_wait(5)
        if self._base_url:
            self.driver.get(self._base_url)


    def find(self, locator: tuple):
        return self.driver.find_element(*locator)

    def finds(self, locator: tuple):
        return self.driver.find_elements(*locator)

    def wait_for_click(self, locator: tuple, timeout=10):
        WebDriverWait(
            self.driver, timeout).until(
            EC.element_to_be_clickable(locator))
