#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/17 13:55
# @Author  : Zy
# @File    : index_page.py
# @Software: PyCharm
from test_selenium.test_weixin1.page.addmember_page import AddMemberPage
from test_selenium.test_weixin1.page.base_page import BasePage
from selenium.webdriver.common.by import By





class IndexPage(BasePage):
    # _base_url = 'https://work.weixin.qq.com/wework_admin/frame#index'
    _base_url = 'https://www.baidu.com'
    def click_add_memeber(self):
        '''
        添加成员
        '''
        add_member_locator = (By.CSS_SELECTOR,
                              '.index_service_cnt_itemWrap:nth-child(1)')
        # self.driver.get(self._base_url)
        self.find(add_member_locator).click()
        return AddMemberPage(self.driver)