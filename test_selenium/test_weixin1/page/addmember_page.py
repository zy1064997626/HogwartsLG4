#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/17 14:09
# @Author  : Zy
# @File    : addmember_page.py
# @Software: PyCharm
from test_selenium.test_weixin1.page.base_page import BasePage
from selenium.webdriver.common.by import By


class AddMemberPage(BasePage):
    def add_member(self, name, account, phonenum):
        name_locator = (By.ID, 'username')
        account_locator = (By.ID, "memberAdd_acctid")
        phonenum_locator = (By.ID, "memberAdd_phone")
        save_locator = (By.CSS_SELECTOR, ".js_btn_save")
        self.find(name_locator).send_keys(name)
        self.find(account_locator).send_keys(account)
        self.find(phonenum_locator).send_keys(phonenum)
        self.find(save_locator).click()
        return True
    def get_member(self, value):
        locator = (By.CSS_SELECTOR, ".ww_checkbox")
        self.wait_for_click(locator)
        while True:
            eles = self.finds((By.CSS_SELECTOR,'.member_colRight_memberTable_td:nth-child(2)'))
            titles = [element.get_attribute("title") for element in eles]
            if value in titles:
                return True
            page: str = self.find((By.CSS_SELECTOR, ".ww_pageNav_info_text")).text
            num, total = page.split("/", 1)
            if int(num) == int(total):
                return False
            else:
                # 翻页
                self.find((By.CSS_SELECTOR, ".ww_commonImg_PageNavArrowRightNormal")).click()


