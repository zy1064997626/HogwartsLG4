#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/12 16:37
# @Author  : Zy
# @File    : test_parametrize.py
# @Software: PyCharm
import pytest


@pytest.mark.parametrize('x', [1, 2])
@pytest.mark.parametrize('y', [8, 10, 11], indirect=True)
def test_foo(x, y):
    print(f"测试数据组合x:{x}, y{y}")
