#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/12 15:50
# @Author  : Zy
# @File    : test_autouse.py
# @Software: PyCharm
import pytest
@pytest.fixture(autouse=True)
def myfixture():
    print('this is myfixture')

class TestAutoUse:
    def test_one(self):
        print('执行test_one')
        assert 1 + 1 == 3
    def test_two(self):
        print('执行test_two')
        assert 1 + 1 == 3
