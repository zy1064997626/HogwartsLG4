#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/12 15:36
# @Author  : Zy
# @File    : conftest.py
# @Software: PyCharm
import pytest


@pytest.fixture(scope='session')
def open():
    print('打开浏览器')
    yield
    print('执行teardown!')
    print('最后关闭浏览器')
