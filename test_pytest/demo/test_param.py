#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/12 16:53
# @Author  : Zy
# @File    : test_param.py
# @Software: PyCharm
from operator import getitem
import pytest

test_user_data = ['Tome', 'Jerry']


@pytest.fixture(scope='module', autouse=True)
def login_r():
    # user = request.param
    return getitem([0, 1], 1)
# 数据处理


@pytest.mark.parametrize('login_r', test_user_data, indirect=True)
def test_login(login_r):
    a = login_r
    print(f'测试用例中login的返回值;{a}')
    assert a != ''


def test_login2(login_r):
    a = login_r
    print(f'测试用例中login的返回值;{a}')
    assert a != ''
