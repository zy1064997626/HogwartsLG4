#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/12 17:20
# @Author  : Zy
# @File    : test_yaml.py
# @Software: PyCharm
import pytest
from pathlib import Path
import yaml


@pytest.mark.parametrize('a,b',
                         yaml.safe_load(Path('./test.yaml').open(encoding='utf-8')))
def test_foo(a, b):
    print(a, b)
