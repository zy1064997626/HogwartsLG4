#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/12 16:03
# @Author  : Zy
# @File    : test_parms.py
# @Software: PyCharm
import pytest
import logging
LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
logging.basicConfig(filename='my.log', level=logging.DEBUG, format=LOG_FORMAT)


@pytest.fixture(params=[1, 2, 3])
def data(request):
    return request.param


def test_not_2(data):
    logging.info('测试数据{}'.format(data))

    assert data < 5
