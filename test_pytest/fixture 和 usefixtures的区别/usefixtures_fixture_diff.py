#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/11 17:35
# @Author  : Zy
# @File    : usefixtures_fixture_diff.py
# @Software: PyCharm
import pytest


@pytest.fixture(autouse=True)
def login_and_logout():
    print('执行登录操作\n')
    yield
    print('执行注销操作')

class TestDemo:

    def test_case1(self):
        print('test_case1')
        assert 1 == 1

    def test_case2(self):
        print('test_case2')
        assert 2 == 2