#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/11 17:33
# @Author  : Zy
# @File    : conftest.py
# @Software: PyCharm
import pytest
from pathlib import Path
import os


@pytest.fixture(scope="function")
def create_userfixture_dir():
    path = "userfixture"
    os.mkdir(path, 770)


# 无返回值
# @pytest.fixture(scope="session")
# def create_fixture_dir():
#     path = "fixture"
#     os.mkdir(path, 770)


# 无返回值
@pytest.fixture(scope="function")
def add_sum_one():
    sum = 5 + 5


# 有返回值
@pytest.fixture(scope="function")
def add_sum_two():
    sum = 5 + 5
    return sum
