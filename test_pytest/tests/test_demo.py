#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/11/3 13:53
# @Author  : Zy
# @File    : test_demo.py
# @Software: PyCharm
import pytest


# @pytest.fixture(params=[0, 1], ids=["spam", "ham"])
# def a(request):
#     return request.param
#
#
# def test_a(a):
#     pass


def idfn(fixture_value):
    if fixture_value == 0:
        return "eggs"
    else:
        return 'dadasdasdas'

@pytest.mark.parametrize
@pytest.fixture(params=[0, 1])
def b(request):
    return request.param

def test_b(b):
    print('b', b)
