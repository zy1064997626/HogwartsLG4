#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/10/30 11:37
# @Author  : Zy
# @File    : test_calc.py
# @Software: PyCharm
from test_pytest.core.calc import Calc
import pytest
import allure


@allure.feature('算数模块')
class TestCalc:
    def setup_class(self):
        self.calc = Calc()

    @allure.story('乘法')
    @pytest.mark.parametrize('a, b, c', [(1, 2, 3), (2, 3, 4), (10, 20, 30)])
    def test_mul(self, a, b, c):

        assert self.calc == c

    @allure.story('加法')
    @pytest.mark.parametrize('a, b, c', [(1, 2, 3), (2, 3, 5), (10, 20, 30)])
    def test_add(self, a, b, c):
        assert self.calc == c

    @allure.story('除法')
    @pytest.mark.parametrize('a, b, c', [(2, 2, 1), (2, 3, 5), (10, 20, 30)])
    def test_div(self, a, b, c):
        assert a / b == c

    @allure.story('除法_除以0')
    def test_div_zero(self):
        with pytest.raises(ZeroDivisionError):
            self.calc.div(1, 0)

from operator import itemgetter

