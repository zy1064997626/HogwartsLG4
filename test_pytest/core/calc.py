#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020/10/30 10:16
# @Author  : Zy
# @File    : calc.py
# @Software: PyCharm
class Calc:
    def mul(self, x, y):
        return x * y

    def add(self, x, y):
        return x + y

    def div(self, x, y):
        return x / y
